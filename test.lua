{
    shell = {
        name = "Shell",
        description = "Just the normal CC Shell",
        dependencies = {},
        version = "1.0",
        files = {
            ["https://raw.githubusercontent.com/SquidDev-CC/CC-Tweaked/master/src/main/resources/assets/computercraft/lua/rom/programs/shell.lua"] = "/programs/shell.lua",
        }
    },
    
    pocketprogs = {
        name = "Pocket Programs",
        description = "All Pocket Programs",
        dependencies = {},
        files = {
            ["https://raw.githubusercontent.com/SquidDev-CC/CC-Tweaked/master/src/main/resources/assets/computercraft/lua/rom/programs/pocket/equip.lua"] = "/programs/equip.lua",
            ["https://raw.githubusercontent.com/SquidDev-CC/CC-Tweaked/master/src/main/resources/assets/computercraft/lua/rom/programs/pocket/falling.lua"] = "/programs/falling.lua",
            ["https://raw.githubusercontent.com/SquidDev-CC/CC-Tweaked/master/src/main/resources/assets/computercraft/lua/rom/programs/pocket/unequip.lua"] = "/programs/unequip.lua",
        },
        version = "1.0",
    },
    
    pastebin = {
        name = "Pastebin",
        description = "The Pastebin Program",
        dependencies = { "shell" },
        files = {
            ["https://raw.githubusercontent.com/SquidDev-CC/CC-Tweaked/master/src/main/resources/assets/computercraft/lua/rom/programs/http/pastebin.lua"] = "/programs/pastebin.lua"
        },
        version = "1.0",
    },

    debtest = {
        name = "Dependency Test",
        description = "Shows dependencies",
        dependencies = { "pastebin", "shell" },
        version = "1.0",
        files = {
        }
    },
    
}