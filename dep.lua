{
    help = {
        name = "Help",
        description = "the help program with a helpfile",
        dependencies = {},
        version = "1.0",
        files = {
            ["https://raw.githubusercontent.com/SquidDev-CC/CC-Tweaked/master/src/main/resources/assets/computercraft/lua/rom/programs/help.lua"] = "/programs/help.lua",
            ["https://raw.githubusercontent.com/SquidDev-CC/CC-Tweaked/master/src/main/resources/assets/computercraft/lua/rom/help/adventure.txt"] = "/help/adventure.txt",
        }
    },
}